import { css } from "@emotion/react";
import Cookies from "cookies";
import { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useUserState } from "../components/Providers/UserStateProvider";
import { UserRegistrationInput, UserLoginInput } from "../shared/generated";
import { Colors } from "../styles/colors";
import { wrappedFetch } from "../utils/ui";

const RootPageStyles = {
  Container: css({
    // Size
    width: "100vw",
    height: "100vh",

    // Content positioning
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    // Colors
    backgroundColor: Colors.BackgroundPrimary,
  }),

  InfoWrapper: {
    Wrapper: css({
      height: "400px",
      display: "flex",
      borderRadius: "4px",
      overflow: "hidden",
    }),
    FormContainer: css({
      display: "flex",
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }),
    Form: css({
      backgroundColor: Colors.BackgroundFloating,
      width: "300px",
      height: "400px",

      display: "flex",
      flexDirection: "column",
      overflow: "hidden",
      h3: {
        flexGrow: 0,
        padding: "14px 25px",
        color: Colors.TextNormal,
      },
      div: {
        flexGrow: 1,
        padding: "14px 25px",
        backgroundColor: Colors.BackgroundTertiary,
      },
    }),

    AboutContainer: css({
      backgroundColor: Colors.BackgroundFloating,
      width: "500px",

      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",

      "*": {
        color: Colors.TextWhite,
      },

      h1: {
        fontSize: "3rem",
        marginBottom: "8px",
      },
      small: {
        color: Colors.TextMuted,
      },
    }),
  },
};

const registerUser = async (
  userInput: UserRegistrationInput,
  onSuccess: () => void
) => {
  const res = await wrappedFetch("POST", "/api/auth/register", userInput, {
    noParse: true,
  });

  if (res.isOk()) {
    onSuccess();
  }
};

const loginUser = async (userInput: UserLoginInput, onSuccess: () => void) => {
  const res = await wrappedFetch("POST", "/api/auth/login", userInput, {
    noParse: true,
  });

  if (res.isOk()) {
    onSuccess();
  }
};

const RootPage: NextPage<{ userLoggedIn: boolean }> = ({ userLoggedIn }) => {
  // Form state
  const [userFormState, setUserFormState] = useState({
    username: "",
    email: "",
    password: "",
  });
  // Existing user = login, not existing = register
  const [existingUser, setExistingUser] = useState(true);

  // TODO: Consume this and show loading spinner when authenticating
  const [loadingAuth, setLoadingAuth] = useState(false);
  // UI state
  const { user } = useUserState();
  const router = useRouter();

  const postAuth = async () => {
    setLoadingAuth(false);
    router.replace("/channels/@me");
  };

  useEffect(() => {
    if (user) {
      router.replace("/channels/@me");
    }
  }, [userLoggedIn, user]);

  return (
    <main css={RootPageStyles.Container}>
      <Head>
        <title>Discordium</title>
      </Head>
      <div css={RootPageStyles.InfoWrapper.Wrapper}>
        <section css={RootPageStyles.InfoWrapper.FormContainer}>
          <div css={RootPageStyles.InfoWrapper.Form}>
            <h3>{existingUser ? "Login to" : "Register for"} Discordium</h3>
            <div>
              <span>
                <label htmlFor="email">Email</label>
                <input
                  onInput={(e) =>
                    setUserFormState({
                      ...userFormState,
                      email: e.currentTarget.value,
                    })
                  }
                  value={userFormState.email}
                  type="email"
                  placeholder="Email"
                  name="email"
                />
              </span>

              {!existingUser && (
                <span>
                  <label htmlFor="username">Username</label>
                  <input
                    onInput={(e) =>
                      setUserFormState({
                        ...userFormState,
                        username: e.currentTarget.value,
                      })
                    }
                    value={userFormState.username}
                    type="text"
                    placeholder="Username"
                    name="username"
                  />
                </span>
              )}
              <span>
                <label htmlFor="password">Password</label>
                <input
                  onInput={(e) =>
                    setUserFormState({
                      ...userFormState,
                      password: e.currentTarget.value,
                    })
                  }
                  value={userFormState.password}
                  type="password"
                  placeholder="Password"
                  name="password"
                />
              </span>
              <button onClick={() => setExistingUser(!existingUser)}>
                {existingUser ? "Register" : "Login"}
              </button>
              <button
                onClick={() => {
                  setLoadingAuth(true);
                  existingUser
                    ? loginUser(
                        {
                          email: userFormState.email,
                          password: userFormState.password,
                        },
                        postAuth
                      )
                    : registerUser(userFormState, postAuth);
                }}
              >
                Submit
              </button>
            </div>
          </div>
        </section>
        <section css={RootPageStyles.InfoWrapper.AboutContainer}>
          <h1>Discordium</h1>
          <p>The "Chromium" of Discord</p>
          <p>
            <small>(some assembly required)</small>
          </p>
        </section>
      </div>
    </main>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  if (req && res) {
    const cookies = new Cookies(req, res);

    // These don't have to be VALID, but their presence means we need to try to get user data
    if (cookies.get("accessToken") && cookies.get("refreshToken")) {
      res.writeHead(302, { location: "/channels/@me" });
      res.end();
    }
  }
  return { props: { userLoggedIn: false } };
};

export default RootPage;
