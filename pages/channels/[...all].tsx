import Cookies from "cookies";
import { GetServerSideProps, NextPage } from "next";
import { LogicWrapper } from "../../components/Layout/LogicWrapper";
import { ServersStateProvider } from "../../components/Providers/ServersProvider";

// Wrapper to allow direct access to Main providers directly from LogicWrapper
const MainStateProviders: NextPage<{ userLoggedIn: boolean }> = ({
  userLoggedIn,
}) => {
  return (
    <ServersStateProvider>
      <LogicWrapper userLoggedIn={userLoggedIn} />
    </ServersStateProvider>
  );
};

// Operates on MainStateProviders
export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  // Only attempt on server
  const cookies = new Cookies(req, res);

  if (cookies.get("accessToken") && cookies.get("refreshToken")) {
    // TODO: Consider putting a token validator here
    return { props: { userLoggedIn: true } };
  }

  return { props: { userLoggedIn: false } };
};

export default MainStateProviders;
