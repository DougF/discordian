import Cookies from "cookies";
import { NextApiRequest, NextApiResponse } from "next";

import { useMongoDB } from "../../../server/utils/db";
import { sendErrorResponse } from "../../../server/utils/responses";
import {
  createAccessToken,
  createRefreshToken,
  SessionManager,
} from "../../../server/utils/sessions";
import { UserRegistrationInput } from "../../../shared/generated";
import { hashPassword } from "../../../server/utils/passwords";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "POST") {
    // Perform any checks on data
    if (!req.body) {
      return res.status(400).send("Missing user data");
    }

    const body: UserRegistrationInput = JSON.parse(req.body);

    try {
      const SM = new SessionManager();
      const { User } = await useMongoDB();
      const createdUser = await User.create({
        ...body,
        password: await hashPassword(body.password),
      });

      const cookies = new Cookies(req, res);
      // Create the first session for this new user
      const sessionId = await SM.createSession({
        userId: createdUser._id,
      });

      const expires = new Date();
      expires.setDate(new Date().getDate() + 100);

      // These will be used in the onboarding process
      cookies.set("refreshToken", createRefreshToken(sessionId), {
        // secure: true,
        expires,
      });
      cookies.set(
        "accessToken",
        createAccessToken(sessionId, createdUser._id),
        {
          // secure: true,
        }
      );

      return res.status(200).end();
    } catch (e) {
      if (e.message.includes("duplicate key error")) {
        return sendErrorResponse(res, {
          status: 409,
          body: "A user with that username or email already exists",
        });
      }
      console.error("[Unhandled Server Error]", e);
      return sendErrorResponse(res, {
        status: 500,
        body: "Unknown error occurred",
      });
    }
  }

  sendErrorResponse(res, {
    status: 405,
    body: "Only POST allowed",
  });
};
