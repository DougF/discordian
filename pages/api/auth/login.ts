import Cookies from "cookies";
import { NextApiRequest, NextApiResponse } from "next";
import { useMongoDB } from "../../../server/utils/db";
import { matchPassword } from "../../../server/utils/passwords";
import { sendErrorResponse } from "../../../server/utils/responses";
import {
  createAccessToken,
  createRefreshToken,
  SessionManager,
} from "../../../server/utils/sessions";
import { UserLoginInput } from "../../../shared/generated";
import { UserDocument } from "../../../server/models/User";

const GenericErrors = {
  invalidCreds: "Email or password invalid",
};

const loginResolver = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "POST") {
    const { User } = await useMongoDB();
    const SM = new SessionManager(),
      body: UserLoginInput = JSON.parse(req.body);

    const user: UserDocument | null = await User.findOne({
      email: body.email,
    }).exec();

    // User doesn't exist or is invalid
    if (!user || !(await matchPassword(body.password, user.password))) {
      return sendErrorResponse(res, {
        status: 401,
        body: GenericErrors.invalidCreds,
      });
    }

    // TODO: Extract this into external util
    const cookies = new Cookies(req, res);
    // Everything is good, proceed with session creation (login)
    const session = await SM.createSession({
      userId: user._id,
    });

    const expires = new Date();
    expires.setDate(new Date().getDate() + 100);

    cookies.set("refreshToken", createRefreshToken(session), {
      // secure: true,
      expires,
    });
    cookies.set("accessToken", createAccessToken(session, user._id), {
      // secure: true,
    });

    return res.status(200).end();
  }
  sendErrorResponse(res, {
    status: 405,
    body: "Only POST method allowed",
  });
};

export default loginResolver;
