import { NextApiRequest, NextApiResponse } from "next";
import { ServerCreationInput } from "../../shared/generated";
import { sendErrorResponse } from "../../server/utils/responses";
import { useMongoDB } from "../../server/utils/db";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== "POST") {
    return sendErrorResponse(res, {
      status: 405,
      body: "Only POST allowed",
    });
  }
  if (!req.body) {
    return sendErrorResponse(res, {
      status: 405,
      body: "Only POST allowed",
    });
  }

  const { User, Server, mg } = await useMongoDB();
  const body: ServerCreationInput & { email: string } = JSON.parse(req.body);

  // Get user reference
  const user = await User.findOne({ email: body.email })
    .select("-password")
    .exec();

  if (!user) {
    return res.status(400).send("Need user");
  }

  // Create a server based on that user + config
  const server = await Server.create({
    name: body.name,
    icon: null,
    roles: [
      {
        name: "@everyone",
        isDefault: true,
        color: "#FFFFFF",
        permissions: [],
      },
    ],
    members: [
      {
        roles: ["@everyone", "@owner"],
        user: new mg.mongo.ObjectId(user._id),
      },
    ],
  });

  res.send({ server });
};
