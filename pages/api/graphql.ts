import {
  ApolloServer,
  gql,
  mergeResolvers,
  mergeTypeDefs,
} from "apollo-server-micro";
import Cookies from "cookies";
import { NextApiRequest, NextApiResponse } from "next";
import { ServerResolvers } from "../../server/resolvers/serverResolvers";
import { UserResolvers } from "../../server/resolvers/userResolvers";
import { GeneralTypeDefs } from "../../server/typeDefs/GeneralTypes";
import { ServerTypeDef } from "../../server/typeDefs/Server";

import { UserTypeDef } from "../../server/typeDefs/User";
import { validateJWT } from "../../server/utils/jwt";
import { createAccessToken } from "../../server/utils/sessions";
import { useMongoDB } from "../../server/utils/db";

const typeDefs = mergeTypeDefs([
  gql`
    type Query {
      helloWorld: String!
    }
  `,
  GeneralTypeDefs,
  UserTypeDef,
  ServerTypeDef,
]);

const resolvers = mergeResolvers([
  {
    Query: {
      helloWorld: () => "Hello, Next + GQL!",
    },
  },
  UserResolvers,
  ServerResolvers,
]);

const parseAccessToken = (token: string | undefined) => {
  try {
    // This will throw if invalid
    return validateJWT(token || "") as {
      sessionId: string;
      userId: string;
    };
  } catch (e) {
    console.error("[Access token]", e.message);
    return undefined;
  }
};

const parseRefreshToken = async (
  token: string | undefined
): Promise<{ userId: string; sessionId: string } | null> => {
  const { Session, mg } = await useMongoDB();
  try {
    const data = validateJWT(token || "") as {
      sessionId: string;
    };

    // Check for existing session and invalidate if missing (remote logout)
    const session = await Session.findOne({
      _id: new mg.mongo.ObjectId(data.sessionId),
    }).exec();

    if (!session) {
      throw new Error("Session ID not present in DB");
    }

    return { userId: session.userId, sessionId: session._id };
  } catch (e) {
    console.error(e);
    return null;
  }
};

const renewRefreshToken = (token: string | undefined, cookies: Cookies) => {
  // Refresh the refreshToken cookie
  const expires = new Date();
  expires.setDate(new Date().getDate() + 100);
  cookies.set("refreshToken", token, {
    // secure: true,
    expires,
  });
};

const handler = new ApolloServer({
  typeDefs,
  resolvers,
  async context({ req, res }: { req: NextApiRequest; res: NextApiResponse }) {
    const cookies = new Cookies(req, res);

    const tokens = {
      accessToken: cookies.get("accessToken"),
      refreshToken: cookies.get("refreshToken"),
    };

    const context = {
      user: "",
      session: "",
    };

    try {
      let userContext = parseAccessToken(tokens.accessToken);
      const refreshTokenData = await parseRefreshToken(tokens.refreshToken);

      // Bad refresh token means don't allow access to ANYTHING
      if (!refreshTokenData) {
        // Setting with no value argument clears the cookie
        cookies.set("refreshToken");
        cookies.set("accessToken");
        return context;
      }

      // Access token is expired, but not refresh token
      if (!userContext) {
        const newAccessToken = createAccessToken(
          refreshTokenData.sessionId,
          refreshTokenData.userId
        );
        cookies.set("accessToken", newAccessToken);

        userContext = {
          sessionId: refreshTokenData.sessionId,
          userId: refreshTokenData.userId,
        };
      }

      // You can only get to this point with a valid refresh token, so renew it
      renewRefreshToken(tokens.refreshToken, cookies);

      // Set context with now validated creds
      context.user = userContext.userId;
      context.session = userContext.sessionId;

      return context;
    } catch (e) {
      // The "useAuth" api hook will handle error throwing on routes that need it
      return context;
    }
  },
}).createHandler({ path: "/api/graphql" });

// Next.js specific, basically a middleware for this serverless "route"
export const config = {
  api: {
    bodyParser: false,
  },
};

export default handler;
