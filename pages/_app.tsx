import type { AppProps } from "next/dist/next-server/lib/router/router";
import Head from "next/head";
import { ApolloProvider } from "../components/Providers/ApolloProvider";
import { UserStateProvider } from "../components/Providers/UserStateProvider";
import "../styles/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider>
      <Head>
        <title>Discordium</title>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css"
          integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY="
          crossOrigin="anonymous"
        />
      </Head>
      <UserStateProvider>
        <Component {...pageProps} />
      </UserStateProvider>
    </ApolloProvider>
  );
}

export default MyApp;
