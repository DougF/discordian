export enum Collections {
  Users = "users",
  Sessions = "sessions",
  Servers = "servers",
  Channels = "channels",
}
