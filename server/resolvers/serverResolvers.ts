import { FieldValue } from "@google-cloud/firestore";
import {
  Server,
  QueryServersArgs,
  QueryChannelsArgs,
  Channel,
  MutationServerArgs,
} from "../../shared/generated";
import { ResolverContext } from "../typeDefs/Context";
import { useMongoDB } from "../utils/db";
import { useAuthentication } from "../utils/hooks";
import { useImageUploader } from "../utils/images";

export const ServerResolvers = {
  Query: {
    // TODO: Add error catchers here
    async servers(
      _parent: any,
      args: QueryServersArgs,
      context: ResolverContext
    ): Promise<Server[]> {
      const { Server, mg } = await useMongoDB();
      const { user } = useAuthentication(context);
      return (await Server.find({
        "members.user": new mg.mongo.ObjectId(user),
      }).exec()) as Server[];
    },

    async channels(
      _parent: any,
      { server }: QueryChannelsArgs,
      context: ResolverContext
    ): Promise<Channel[]> {
      const { user } = useAuthentication(context);
      const { Channel } = await useMongoDB();

      // TODO: Block channel query if user not present in members

      const channels = await Channel.find({
        serverId: server,
      }).exec();

      console.log("[channels]", channels);
      return channels.map((channel) => ({
        _id: channel._id,
        serverId: channel.serverId,
        name: channel.name,
        isDefault: channel.isDefault,
      }));
    },
  },
  Mutation: {
    async server(
      _parent: unknown,
      { config }: MutationServerArgs,
      context: ResolverContext
    ): Promise<Server> {
      const { user } = useAuthentication(context);
      const { Server, Channel, mg } = await useMongoDB();

      // Create a server based on that user + config
      const server = await Server.create({
        name: config.name,
        icon: null,
        roles: [
          {
            name: "@everyone",
            isDefault: true,
            color: "#FFFFFF",
            permissions: [],
          },
        ],
        members: [
          {
            roles: ["@everyone", "@owner"],
            user: new mg.mongo.ObjectId(user),
          },
        ],
      });

      await Channel.create({
        serverId: server._id,
        name: config.defaultChannel,
        isDefault: true,
      });

      return {
        _id: server._id,
        name: server.name,
        roles: server.roles,
        members: server.members,
      };
    },
  },
};
