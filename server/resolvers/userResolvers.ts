import { ApolloError } from "apollo-server-errors";
import { QueryResolvers, User as UserType } from "../../shared/generated";
import { ResolverContext } from "../typeDefs/Context";
import { useMongoDB } from "../utils/db";
import { UserDocument } from "../models/User";
import { useAuthentication } from "../utils/hooks";
interface UserResolversType {
  Query: Pick<QueryResolvers, "me">;
}

export const UserResolvers = {
  Query: {
    async me(_parent, _args, context: ResolverContext): Promise<UserType> {
      try {
        const { User, mg } = await useMongoDB();
        const { user } = useAuthentication(context);

        const userDoc: UserDocument | null = await User.findOne({
          _id: new mg.mongo.ObjectId(user),
        })
          .select("-password")
          .exec();

        if (!userDoc) {
          // Should not happen unless user gets deleted
          throw new ApolloError("Cannot find user with provided ID");
        }
        // This gets around _id technically being optional on `UserDocument`
        return {
          _id: userDoc._id,
          username: userDoc.username,
          email: userDoc.email,
        };
      } catch (e) {
        console.error(e);
        throw new ApolloError("Something went wrong retrieving user data");
      }
    },
  } as UserResolversType["Query"],
};
