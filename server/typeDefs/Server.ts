import { gql } from "apollo-server-micro";

export const ServerTypeDef = gql`
  # Definitions
  # ==============================
  type Server {
    _id: ID!
    icon: Icon
    name: String!
    roles: [ServerRole!]!
    members: [ServerMember!]!
    # Permissions will come later
  }

  type ServerMember {
    user: ID!
    roles: [String!]!
  }

  type ServerRole {
    name: String!
    color: String
    permissions: [String!]!
  }

  input ServerCreationInput {
    icon: String!
    name: String!
    defaultChannel: String!
  }

  type Channel {
    _id: ID!
    serverId: ID!
    name: String!
    isDefault: Boolean!
  }

  input ChannelCreationInput {
    name: String!
  }

  # Retrieval Methods
  # ==============================
  input ServerSearchConfig {
    # byUser: ID
    byName: String
  }

  input ChannelFilter {
    roles: [String!]
  }

  type Query {
    servers(search: ServerSearchConfig): [Server!]!
    roles(server: ID!): [ServerRole!]!
    members(server: ID!): [ServerMember!]!
    channels(server: ID!, filter: ChannelFilter): [Channel!]!
  }

  type Mutation {
    server(config: ServerCreationInput!): Server
    channel(server: ID!, config: ChannelCreationInput!): Channel
  }
`;
