import { gql } from "apollo-server-core";

export const GeneralTypeDefs = gql`
  type Icon {
    isStatic: Boolean!
    url: String!
    # Dynamic only present when isStatic is false
    dynamicUrl: String
  }
`;
