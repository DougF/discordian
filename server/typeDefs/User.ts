import { gql } from "apollo-server-core";

export const UserTypeDef = gql`
  type User {
    _id: ID!
    icon: Icon
    username: String!
    password: String
    email: String!
  }

  input UserRegistrationInput {
    username: String!
    email: String!
    password: String!
  }

  input UserLoginInput {
    email: String!
    password: String!
  }

  type Query {
    me: User!
  }
`;
