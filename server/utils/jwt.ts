import jwt from "jsonwebtoken";

const useJWTSecret = () => {
  const secret = process.env.JWT_SECRET;

  if (!secret) {
    throw new Error("[MISSING ENV] Cannot find JWT_SECRET in environment");
  }

  return secret;
};

export const encodeJWT = (
  payload: Record<string, unknown>,
  expiresIn = "90 min"
) => {
  const secret = useJWTSecret();
  return jwt.sign(payload, secret, {
    expiresIn,
  });
};

export const validateJWT = (token: string) => {
  const secret = useJWTSecret();
  return jwt.verify(token, secret);
};
