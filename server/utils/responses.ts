import { NextApiResponse } from "next";

export const sendErrorResponse = (
  res: NextApiResponse,
  options: {
    status?: number;
    statusMessage?: string;
    body?: any;
  }
) => {
  const code = options.status || 500;
  // No body, just use the code and end
  if (!options.body) {
    return res.status(code).end();
  }

  // Otherwise construct a proper response
  res.status(code);
  if (options.statusMessage) {
    res.statusMessage = options.statusMessage;
  }

  res.send({
    code,
    error: options.body,
  });
};
