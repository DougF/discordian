import mongoose from "mongoose";
import { Storage } from "@google-cloud/storage";
import { Session, sessionSchema } from "../models/Session";
import { UserDocument, userSchema } from "../models/User";
import { ServerDocument, serverSchema } from "../models/Server";
import { ChannelDocument, channelSchema } from "../models/Channel";

// export const connectToFirestore = (): Firestore => {
//   if (
//     !process.env.GCP_PROJECT_ID ||
//     !process.env.GCP_FIRESTORE_CLIENT_EMAIL ||
//     !process.env.GCP_FIRESTORE_PRIVATE_KEY
//   ) {
//     throw new Error(
//       "[Firestore Error] Missing GCP Firestore credentials, cannot create client"
//     );
//   }
//   return new Firestore({
//     projectId: process.env.GCP_PROJECT_ID,
//     credentials: {
//       client_email: process.env.GCP_FIRESTORE_CLIENT_EMAIL,
//       private_key: process.env.GCP_FIRESTORE_PRIVATE_KEY,
//     },
//   });
// };

let mongooseRef: typeof mongoose;

export const useMongoDB = async (): Promise<{
  mg: typeof mongoose;
  Session: mongoose.Model<Session, {}>;
  User: mongoose.Model<UserDocument, {}>;
  Server: mongoose.Model<ServerDocument, {}>;
  Channel: mongoose.Model<ChannelDocument, {}>;
}> => {
  if (!process.env.MONGO_CONNECTION_STRING) {
    throw new Error(
      "[MongoDB Error] Missing MongoDB credential string, cannot create client"
    );
  }

  // Only use one DB connection or else you'll create a new one for each request
  const mg = mongooseRef
    ? mongooseRef
    : await mongoose.connect(process.env.MONGO_CONNECTION_STRING, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });

  return {
    mg,
    Session:
      mongoose.models.Session ||
      mongoose.model<Session>("Session", sessionSchema),
    User:
      mongoose.models.User || mongoose.model<UserDocument>("User", userSchema),
    Server:
      mongoose.models.Server ||
      mongoose.model<ServerDocument>("Server", serverSchema),
    Channel:
      mongoose.models.Channel ||
      mongoose.model<ChannelDocument>("Channel", channelSchema),
  };
};

export const useCloudStorage = () => {
  if (
    !process.env.GCP_PROJECT_ID ||
    !process.env.GCP_CLOUD_STORAGE_CLIENT_EMAIL ||
    !process.env.GCP_CLOUD_STORAGE_PRIVATE_KEY
  ) {
    throw new Error(
      "[Cloud Storage Error] Missing GCP Cloud Storage credentials, cannot create client"
    );
  }
  return new Storage({
    projectId: process.env.GCP_PROJECT_ID,
    credentials: {
      client_email: process.env.GCP_CLOUD_STORAGE_CLIENT_EMAIL,
      private_key: process.env.GCP_CLOUD_STORAGE_PRIVATE_KEY,
    },
  });
};
