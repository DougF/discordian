import { AuthenticationError } from "apollo-server-errors";
import { ResolverContext } from "../typeDefs/Context";

export const useAuthentication = (context: ResolverContext) => {
  if (!context.user) {
    throw new AuthenticationError(
      "Token invalid or expired. Cannot get user id"
    );
  }

  return { user: context.user };
};
