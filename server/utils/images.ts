import { useCloudStorage } from "./db";
import stream from "stream";

export const useImageUploader = async (
  fileName: string,
  fileContents: string
): Promise<() => Promise<boolean>> => {
  const storage = useCloudStorage(),
    dataStream = new stream.PassThrough(),
    // TODO: Find a final name sceme instead of this testing one
    file = storage.bucket("discordium-dev-icons").file(fileName);

  // Push contents to stream buffer
  dataStream.push(fileContents);
  // Finish buffer
  dataStream.push(null);

  return async () => {
    return await new Promise((resolve, reject) => {
      dataStream
        .pipe(
          file.createWriteStream({
            resumable: false,
            validation: false,
          })
        )
        .on("error", (e: Error) => {
          reject(e);
        })
        .on("finish", () => {
          resolve(true);
        });
    });
  };
};
