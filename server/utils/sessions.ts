import { useMongoDB } from "./db";
import { encodeJWT } from "./jwt";

export type SessionInput = Omit<Session, "id">;

export interface Session {
  id: string;
  userId: string;
  deviceName?: string;
}

export class SessionManager {
  public async createSession(config: SessionInput): Promise<string> {
    const { Session } = await useMongoDB();
    const session = await Session.create({ userId: config.userId });

    return session._id;
  }

  public async getSessions(userId: string): Promise<Session[]> {
    const { Session } = await useMongoDB()
    const sessions = await Session.find({ userId }).exec()
    return sessions.map(session => ({
      id: session._id,
      userId: session.userId
    }))
  }

  public async deleteSession(id: string): Promise<string> {
    const { Session, mg } = await useMongoDB();
    await Session.deleteOne({
      _id: new mg.mongo.ObjectId(id),
    });

    return id;
  }
}

// This is used as a "Session token"
export const createRefreshToken = (sessionId: string) =>
  encodeJWT(
    {
      sessionId,
    },
    "2 years"
  );

// This is the more traditional JWT, except it only lasts 90 minutes
export const createAccessToken = (sessionId: string, userId: string) =>
  encodeJWT({
    sessionId,
    userId,
  });
