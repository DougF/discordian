import mongoose, { Document } from "mongoose";

export interface ChannelDocument extends Document {
  serverId: string;
  name: string;
  isDefault: boolean;
}

export const channelSchema = new mongoose.Schema<ChannelDocument>({
  serverId: {
    type: mongoose.Types.ObjectId,
    ref: "Server",
    required: true,
  },
  name: { type: String, required: true },
  isDefault: { type: Boolean, default: false, required: true },
});
