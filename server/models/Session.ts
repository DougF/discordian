import mongoose, { Document } from "mongoose";
import { UserDocument } from "./User";

export interface Session extends Document {
  userId: UserDocument["id"];
}

export const sessionSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
});
