import mongoose, * as Mongoose from "mongoose";
import { Document } from "mongoose";
import { Icon, ServerMember, ServerRole } from "../../shared/generated";

export type RoleDocument = Document & Omit<ServerRole, "__typename">;

export interface ServerDocument extends Document {
  name: string;
  icon: Omit<Icon, "__typename">;
  roles: RoleDocument[];
  // Needs to be ObjectId on schema
  members: Array<ServerMember>;
}

export const ServerRoleSchema = new Mongoose.Schema<RoleDocument>({
  name: String,
  isDefault: Boolean,
  color: String,
  permissions: {
    type: Array,
    of: String,
  },
});

export const serverSchema = new mongoose.Schema<ServerDocument>({
  name: String,
  icon: {
    type: Object,
    default: undefined,
  },
  roles: [Object],
  members: [Object],
});
