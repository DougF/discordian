import mongoose, { Document } from "mongoose";
import { Icon } from "../../shared/generated";

export interface UserDocument extends Document {
  username: string;
  password: string;
  icon: Omit<Icon, "__typename">;
  email: string;
}

export const userSchema = new mongoose.Schema<UserDocument>({
  username: { type: String, required: true },
  password: { type: String, required: true },
  icon: {
    type: Object,
  },
  email: {
    required: true,
    type: String,
    unique: true,
  },
});
