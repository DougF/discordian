import { Result, err, ok } from "@dukeferdinand/ts-results";

export const wrappedFetch = async <T, E = string>(
  method: string,
  uri: string,
  body: unknown,
  misc?: {
    noParse?: boolean;
  }
): Promise<Result<T, E>> => {
  return await fetch(`${uri}`, {
    method,
    body: typeof body === "string" ? body : JSON.stringify(body),
  }).then(async (res) => {
    if (!res.ok) {
      if (misc?.noParse) {
        return err("Got err, noParse selected");
      }
      return err(await res.json());
    }

    if (misc?.noParse) {
      return ok("Got ok, noParse selected");
    }
    return ok(await res.json());
  });
};
