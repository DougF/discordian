export const getCookies = <T extends Record<string, any>>(
  cookieString?: string
): Partial<T> => {
  const pairs =
    // Passed in string
    cookieString?.split(";") ||
    // Else try window
    (typeof window !== "undefined" && window.document.cookie.split(";")) ||
    // Default to empty array
    [];
  const cookies: Record<string, unknown> = {};

  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split("=");
    if ((pair[0] + "").trim() === "") {
      console.warn(
        "[ COOKIE PARSE ] Got empty key when parsing cookies, please check you have set them properly"
      );

      //? If we don't break here, a useless object is returned that looks something like this:
      //? {
      //?    "": "undefined"
      //? }
      break;
    }

    cookies[(pair[0] + "").trim()] = unescape(pair[1]);
  }
  return cookies as Partial<T>;
};
