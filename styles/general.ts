export enum Shadows {
  ElevationStroke = "0 0 0 1px rgba(4,4,5,0.15)",
  ElevationHigh = "0 8px 16px rgba(0,0,0,0.24)",
}

export enum Transitions {
  Standard = "all 175ms ease-out",
}
