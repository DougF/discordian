// /* Discord style theme */
// --background-primary: #36393f;
// --background-secondary: #2f3136;
// --background-secondary-alt: #292b2f;
// --background-tertiary: #202225;
// --background-accent: #4f545c;
// --background-floating: #18191c;
// --background-mobile-primary: #36393f;
// --background-mobile-secondary: #2f3136;
// --background-modifier-hover: rgba(79, 84, 92, 0.16);
// --background-modifier-accent: hsla(0, 0%, 100%, 0.06);

export const Colors = {
  // Text
  TextWhite: "#ffffff",
  TextNormal: "#dcddde",
  TextMuted: "#72767d",
  TextGreen: "#43b581",

  // Backgrounds
  BackgroundPrimary: "#36393f",
  BackgroundSecondary: "#2f3136",
  BackgroundSecondaryAlt: "#292b2f",
  BackgroundTertiary: "#202225",
  BackgroundAccent: "#4f545c",
  BackgroundModifierHover: "rgba(79, 84, 92, 0.16)",
  BackgroundModifierAccent: "hsla(0, 0%, 100%, 0.06)",
  BackgroundFloating: "#18191c",
  BackgroundFooter: "#2f3136",
  BackgroundForm: "#36393f",

  // Misc
  DiscordBlue: "#7289da",

  Light: {
    BackgroundPrimary: "#ffffff",
    HeaderPrimary: "#060607",
    HeaderSecondary: "#4f5660",
    TextMuted: "#747f8d",
  },
};
