import {
  GraphQLResolveInfo,
  GraphQLScalarType,
  GraphQLScalarTypeConfig,
} from "graphql";
import { gql } from "@apollo/client";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = {
  [X in Exclude<keyof T, K>]?: T[X];
} &
  { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export enum CacheControlScope {
  Public = "PUBLIC",
  Private = "PRIVATE",
}

export type Channel = {
  __typename?: "Channel";
  _id: Scalars["ID"];
  serverId: Scalars["ID"];
  name: Scalars["String"];
  isDefault: Scalars["Boolean"];
};

export type ChannelCreationInput = {
  name: Scalars["String"];
};

export type ChannelFilter = {
  roles?: Maybe<Array<Scalars["String"]>>;
};

export type Icon = {
  __typename?: "Icon";
  isStatic: Scalars["Boolean"];
  url: Scalars["String"];
  dynamicUrl?: Maybe<Scalars["String"]>;
};

export type Mutation = {
  __typename?: "Mutation";
  server?: Maybe<Server>;
  channel?: Maybe<Channel>;
};

export type MutationServerArgs = {
  config: ServerCreationInput;
};

export type MutationChannelArgs = {
  server: Scalars["ID"];
  config: ChannelCreationInput;
};

export type Query = {
  __typename?: "Query";
  helloWorld: Scalars["String"];
  me: User;
  servers: Array<Server>;
  roles: Array<ServerRole>;
  members: Array<ServerMember>;
  channels: Array<Channel>;
};

export type QueryServersArgs = {
  search?: Maybe<ServerSearchConfig>;
};

export type QueryRolesArgs = {
  server: Scalars["ID"];
};

export type QueryMembersArgs = {
  server: Scalars["ID"];
};

export type QueryChannelsArgs = {
  server: Scalars["ID"];
  filter?: Maybe<ChannelFilter>;
};

export type Server = {
  __typename?: "Server";
  _id: Scalars["ID"];
  icon?: Maybe<Icon>;
  name: Scalars["String"];
  roles: Array<ServerRole>;
  members: Array<ServerMember>;
};

export type ServerCreationInput = {
  icon: Scalars["String"];
  name: Scalars["String"];
  defaultChannel: Scalars["String"];
};

export type ServerMember = {
  __typename?: "ServerMember";
  user: Scalars["ID"];
  roles: Array<Scalars["String"]>;
};

export type ServerRole = {
  __typename?: "ServerRole";
  name: Scalars["String"];
  color?: Maybe<Scalars["String"]>;
  permissions: Array<Scalars["String"]>;
};

export type ServerSearchConfig = {
  byName?: Maybe<Scalars["String"]>;
};

export type User = {
  __typename?: "User";
  _id: Scalars["ID"];
  icon?: Maybe<Icon>;
  username: Scalars["String"];
  password?: Maybe<Scalars["String"]>;
  email: Scalars["String"];
};

export type UserLoginInput = {
  email: Scalars["String"];
  password: Scalars["String"];
};

export type UserRegistrationInput = {
  username: Scalars["String"];
  email: Scalars["String"];
  password: Scalars["String"];
};

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> =
  | LegacyStitchingResolver<TResult, TParent, TContext, TArgs>
  | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> {
  subscribe: SubscriptionSubscribeFn<
    { [key in TKey]: TResult },
    TParent,
    TContext,
    TArgs
  >;
  resolve?: SubscriptionResolveFn<
    TResult,
    { [key in TKey]: TResult },
    TContext,
    TArgs
  >;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<
  TResult,
  TKey extends string,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
  obj: T,
  context: TContext,
  info: GraphQLResolveInfo
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  CacheControlScope: CacheControlScope;
  Channel: ResolverTypeWrapper<Channel>;
  ID: ResolverTypeWrapper<Scalars["ID"]>;
  String: ResolverTypeWrapper<Scalars["String"]>;
  Boolean: ResolverTypeWrapper<Scalars["Boolean"]>;
  ChannelCreationInput: ChannelCreationInput;
  ChannelFilter: ChannelFilter;
  Icon: ResolverTypeWrapper<Icon>;
  Mutation: ResolverTypeWrapper<{}>;
  Query: ResolverTypeWrapper<{}>;
  Server: ResolverTypeWrapper<Server>;
  ServerCreationInput: ServerCreationInput;
  ServerMember: ResolverTypeWrapper<ServerMember>;
  ServerRole: ResolverTypeWrapper<ServerRole>;
  ServerSearchConfig: ServerSearchConfig;
  Upload: ResolverTypeWrapper<Scalars["Upload"]>;
  User: ResolverTypeWrapper<User>;
  UserLoginInput: UserLoginInput;
  UserRegistrationInput: UserRegistrationInput;
  Int: ResolverTypeWrapper<Scalars["Int"]>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Channel: Channel;
  ID: Scalars["ID"];
  String: Scalars["String"];
  Boolean: Scalars["Boolean"];
  ChannelCreationInput: ChannelCreationInput;
  ChannelFilter: ChannelFilter;
  Icon: Icon;
  Mutation: {};
  Query: {};
  Server: Server;
  ServerCreationInput: ServerCreationInput;
  ServerMember: ServerMember;
  ServerRole: ServerRole;
  ServerSearchConfig: ServerSearchConfig;
  Upload: Scalars["Upload"];
  User: User;
  UserLoginInput: UserLoginInput;
  UserRegistrationInput: UserRegistrationInput;
  Int: Scalars["Int"];
};

export type CacheControlDirectiveArgs = {
  maxAge?: Maybe<Scalars["Int"]>;
  scope?: Maybe<CacheControlScope>;
};

export type CacheControlDirectiveResolver<
  Result,
  Parent,
  ContextType = any,
  Args = CacheControlDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type ChannelResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Channel"] = ResolversParentTypes["Channel"]
> = {
  _id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  serverId?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  name?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  isDefault?: Resolver<ResolversTypes["Boolean"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type IconResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Icon"] = ResolversParentTypes["Icon"]
> = {
  isStatic?: Resolver<ResolversTypes["Boolean"], ParentType, ContextType>;
  url?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  dynamicUrl?: Resolver<
    Maybe<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Mutation"] = ResolversParentTypes["Mutation"]
> = {
  server?: Resolver<
    Maybe<ResolversTypes["Server"]>,
    ParentType,
    ContextType,
    RequireFields<MutationServerArgs, "config">
  >;
  channel?: Resolver<
    Maybe<ResolversTypes["Channel"]>,
    ParentType,
    ContextType,
    RequireFields<MutationChannelArgs, "server" | "config">
  >;
};

export type QueryResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Query"] = ResolversParentTypes["Query"]
> = {
  helloWorld?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  me?: Resolver<ResolversTypes["User"], ParentType, ContextType>;
  servers?: Resolver<
    Array<ResolversTypes["Server"]>,
    ParentType,
    ContextType,
    RequireFields<QueryServersArgs, never>
  >;
  roles?: Resolver<
    Array<ResolversTypes["ServerRole"]>,
    ParentType,
    ContextType,
    RequireFields<QueryRolesArgs, "server">
  >;
  members?: Resolver<
    Array<ResolversTypes["ServerMember"]>,
    ParentType,
    ContextType,
    RequireFields<QueryMembersArgs, "server">
  >;
  channels?: Resolver<
    Array<ResolversTypes["Channel"]>,
    ParentType,
    ContextType,
    RequireFields<QueryChannelsArgs, "server">
  >;
};

export type ServerResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["Server"] = ResolversParentTypes["Server"]
> = {
  _id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  icon?: Resolver<Maybe<ResolversTypes["Icon"]>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  roles?: Resolver<
    Array<ResolversTypes["ServerRole"]>,
    ParentType,
    ContextType
  >;
  members?: Resolver<
    Array<ResolversTypes["ServerMember"]>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ServerMemberResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ServerMember"] = ResolversParentTypes["ServerMember"]
> = {
  user?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  roles?: Resolver<Array<ResolversTypes["String"]>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ServerRoleResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["ServerRole"] = ResolversParentTypes["ServerRole"]
> = {
  name?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  color?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  permissions?: Resolver<
    Array<ResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface UploadScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes["Upload"], any> {
  name: "Upload";
}

export type UserResolvers<
  ContextType = any,
  ParentType extends ResolversParentTypes["User"] = ResolversParentTypes["User"]
> = {
  _id?: Resolver<ResolversTypes["ID"], ParentType, ContextType>;
  icon?: Resolver<Maybe<ResolversTypes["Icon"]>, ParentType, ContextType>;
  username?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  password?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  email?: Resolver<ResolversTypes["String"], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  Channel?: ChannelResolvers<ContextType>;
  Icon?: IconResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Server?: ServerResolvers<ContextType>;
  ServerMember?: ServerMemberResolvers<ContextType>;
  ServerRole?: ServerRoleResolvers<ContextType>;
  Upload?: GraphQLScalarType;
  User?: UserResolvers<ContextType>;
};

/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
export type DirectiveResolvers<ContextType = any> = {
  cacheControl?: CacheControlDirectiveResolver<any, any, ContextType>;
};

/**
 * @deprecated
 * Use "DirectiveResolvers" root object instead. If you wish to get "IDirectiveResolvers", add "typesPrefix: I" to your config.
 */
export type IDirectiveResolvers<
  ContextType = any
> = DirectiveResolvers<ContextType>;
