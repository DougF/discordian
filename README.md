## Discordium

Better README coming soon!

### Env requirements

- NodeJS + npm (relatively recent versions)
- Caddy installed (and able to bind to 443 without sudo)
  - on Linux systems with systemd: `sudo setcap CAP_NET_BIND_SERVICE=+eip $(which caddy)` ([source](https://serverfault.com/a/807884))

### How to run
