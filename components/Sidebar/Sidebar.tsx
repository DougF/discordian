import { css } from "@emotion/react";
import React from "react";
import { Colors } from "../../styles/colors";

const SidebarStyles = {
  Wrapper: css({
    background: Colors.BackgroundSecondary,
    borderRadius: "10px 0 0 0",
    width: "240px",
    height: "100vh",
  }),
};

export const Sidebar: React.FC = () => {
  return (
    <aside css={SidebarStyles.Wrapper}>
      Sidebar <i className="fa fa-angle-down"></i>
    </aside>
  );
};
