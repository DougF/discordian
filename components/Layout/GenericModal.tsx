import React from "react";
import { css } from "@emotion/react";
import { CSSTransition } from "react-transition-group";
import { Colors } from "../../styles/colors";

const GenericModalStyles = {
  Wrapper: css([
    {
      // Position self
      position: "fixed",
      width: "100vw",
      height: "100vh",
      left: 0,
      top: 0,
      right: 0,
      margin: "0 auto",
      zIndex: 5,

      // Position contents
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    // Dialog transitions
    css`
      .generic-dialog {
        transition: all 200ms ease-out;
      }
      &.generic-modal-enter .generic-dialog {
        margin-top: -50px;
        opacity: 0;
      }
      &.generic-modal-enter-active .generic-dialog {
        bacgkround: 0px;
        opacity: 1;
      }
      &.generic-modal-exit .generic-dialog {
        margin-top: 0px;
        opacity: 1;
      }
      &.generic-modal-exit-active .generic-dialog {
        margin-top: -50px;
        opacity: 0;
      }
    `,
  ]),

  ContentWrapper: css({
    width: "100%",
    height: "100%",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  }),

  CloseTrigger: css({
    position: "absolute",
    background: "rgba(0, 0, 0, .85) none repeat scroll 0% 0%",
    width: "100%",
    height: "100%",
  }),

  Modal: css({
    position: "absolute",
    left: 0,
    right: 0,
    margin: "0 auto",
    borderRadius: 5,
    padding: 0,
    border: 0,
    backgroundColor: Colors.BackgroundFloating,
    overflow: "hidden",
  }),
};

interface GenericModalProps {
  open: boolean;
  width: number;
  height: number;
  onClose: () => void;
}

export const GenericModal: React.FC<GenericModalProps> = ({
  children,
  open,
  onClose,
  width,
  height,
}) => {
  return (
    <>
      <CSSTransition
        in={open}
        timeout={100}
        classNames="generic-modal"
        // Clears form state if any
        unmountOnExit
      >
        <div css={GenericModalStyles.Wrapper}>
          <div css={GenericModalStyles.ContentWrapper}>
            <div css={GenericModalStyles.CloseTrigger} onClick={onClose}></div>
            <div
              style={{ width, height }}
              role="dialog"
              className="generic-dialog"
              css={GenericModalStyles.Modal}
            >
              {children}
            </div>
          </div>
        </div>
      </CSSTransition>
    </>
  );
};
