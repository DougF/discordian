import React from "react";
import { css } from "@emotion/react";

import { Colors } from "../../styles/colors";
import { ServerBar } from "../ServerBar/ServerBar";
import { Sidebar } from "../Sidebar/Sidebar";
import { Main } from "../Main/Main";

const LayoutStyles = {
  AppWrapper: css({
    background: Colors.BackgroundTertiary,
    display: "flex",
    height: "100vh",
    width: "100vw",
  }),
};

export const Layout: React.FC = () => {
  return (
    <div css={LayoutStyles.AppWrapper}>
      <ServerBar />
      <Sidebar />
      <Main />
    </div>
  );
};
