import { useEffect } from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";
import { css } from "@emotion/react";

import { Layout } from "./Layout";
import { useUserState } from "../Providers/UserStateProvider";
import { Query } from "../../shared/generated";
import { useServersState } from "../Providers/ServersProvider";
import { Colors } from "../../styles/colors";

const LogicWrapperStyles = {
  AppLoader: css({
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",

    height: "100vh",
    width: "100vw",

    backgroundColor: Colors.BackgroundPrimary,
    color: Colors.TextMuted,
  }),

  LoadingMessageWrapper: css({
    backgroundColor: Colors.BackgroundSecondary,
    padding: "4rem",
    borderRadius: 4,

    h1: {
      marginBottom: "5px",
      textAlign: "center",
    },

    p: {
      fontSize: 15,
      textAlign: "center",
    },
  }),
};

const FIRST_LOAD_QUERY = gql`
  query FirstLoadQuery {
    me {
      _id
      username
      email
    }
    servers {
      _id
      name
      icon {
        isStatic
        url
      }
    }
  }
`;

export const LogicWrapper: React.FC<{ userLoggedIn: boolean }> = ({
  userLoggedIn,
}) => {
  // API
  const { data, loading, error } = useQuery<Query>(FIRST_LOAD_QUERY);

  // Global state
  const { user, setUser } = useUserState();
  const { setServers } = useServersState();
  const router = useRouter();

  useEffect(() => {
    // No potential user authentication methods present, redirect
    if ((!user && !userLoggedIn) || error) {
      router.replace("/");
    }
    if (data) {
      const { me, servers } = data;
      setUser(me);
      setServers(servers);
    }
  }, [user, userLoggedIn, loading, data]);

  // Add more conditions here, such as a second wait for channels to populate
  const appLoading = loading;
  if (appLoading) {
    return (
      <div css={LogicWrapperStyles.AppLoader}>
        <div css={LogicWrapperStyles.LoadingMessageWrapper}>
          <h1>Discordium</h1>

          <p>Connecting to the interwebs</p>
        </div>
        <i className="fa fa-circle-notch" />
      </div>
    );
  }
  return <Layout />;
};
