import React from "react";
import { css } from "@emotion/react";
import Link from "next/link";
import { Colors } from "../../styles/colors";
import { Transitions } from "../../styles/general";

const ServerListItemStyles = {
  ListItem: css({
    width: "72px",
    height: "48px",
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",

    marginBottom: 8,

    ".pill-wrapper": {
      height: "100%",
      display: "flex",
      alignItems: "center",
      position: "absolute",
      left: 0,

      ".pill": {
        marginLeft: "-4px",
        backgroundColor: "white",
        borderRadius: "0 4px 4px 0;",
        transition: Transitions.Standard,
        height: 0,
        width: 0,
      },
    },

    "&:hover": {
      ".pill": {
        height: 20,
        width: 8,
      },
    },

    "&:hover, &.active": {
      ".root-server": {
        backgroundColor: Colors.DiscordBlue,
        i: {
          color: Colors.TextWhite,
        },
      },
      ".server-icon": {
        borderRadius: "15px",
      },
      ".add-server": {
        backgroundColor: Colors.TextGreen,
        i: {
          color: Colors.TextWhite,
        },
      },
    },

    "&.active": {
      ".pill": {
        height: 40,
        width: 8,
      },
    },
  }),

  ServerIcon: css({
    borderRadius: "50%",
    color: "white",
    backgroundColor: Colors.DiscordBlue,

    width: 48,
    height: 48,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    transition: Transitions.Standard,
    overflow: "hidden",

    i: {
      fontSize: 18,
    },

    img: {
      height: 48,
    },
    "&.root-server": {
      backgroundColor: Colors.BackgroundPrimary,
      i: {
        fontSize: 20,
        transition: Transitions.Standard,
        color: Colors.TextNormal,
      },
    },

    "&.add-server": {
      backgroundColor: Colors.BackgroundPrimary,
      i: {
        fontSize: 15,
        color: Colors.TextGreen,
        transition: Transitions.Standard,
      },
    },
  }),

  AddServerIcon: css({}),
};

interface ServerListItemProps {
  icon?: string;
  serverId?: string;
  isActive: boolean;
  isAddServer?: boolean;
  onClick?: () => void;
}

// Forward ref needed in order to attach events to function component
const ServerListItemInner: React.FC<ServerListItemProps> = ({
  onClick,
  isActive,
  isAddServer,
  icon,
  serverId,
}) => (
  <div
    onClick={onClick}
    className={isActive ? "active" : undefined}
    css={ServerListItemStyles.ListItem}
  >
    {/* Active indicator */}
    <div className="pill-wrapper">
      <div className="pill"></div>
    </div>
    {/* Icon wrapper */}
    <div
      className={`
        server-icon
        ${isAddServer ? "add-server" : ""}
        ${serverId === "@me" ? "root-server" : ""}
      `}
      css={ServerListItemStyles.ServerIcon}
    >
      {/* Icon itself */}
      {icon ? (
        <img src={`${process.env.NEXT_PUBLIC_GCP_ICON_BUCKET}/${icon}`} />
      ) : (
        <i className={`fa fa-${isAddServer ? "plus" : "social-home"}`}></i>
      )}
    </div>
  </div>
);

export const ServerListItem: React.FC<ServerListItemProps> = (props) => {
  // If it's not a server link, use the click action
  return props.isAddServer ? (
    <ServerListItemInner {...props} />
  ) : (
    <Link href={`/channels/${props.serverId}`}>
      <span>
        <ServerListItemInner {...props} />
      </span>
    </Link>
  );
};
