import React from "react";
import { useRouter } from "next/router";
import { ServerListItem } from "./ServerListItem";

export const UpperList: React.FC = () => {
  const router = useRouter();
  const currentServer = router.query.all ? router.query.all[0] : "no match";
  return (
    <div>
      <ServerListItem serverId="@me" isActive={currentServer === "@me"} />
    </div>
  );
};
