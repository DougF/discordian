import React, { useReducer } from "react";
import { css } from "@emotion/react";
import { Colors } from "../../styles/colors";
import { ServerList } from "./ServerList";
import { ServerListItem } from "./ServerListItem";
import { UpperList } from "./UpperList";
import { GenericModal } from "../Layout/GenericModal";

const ServerBarStyles = {
  Wrapper: css({
    background: Colors.BackgroundTertiary,
    width: "72px",
    paddingTop: "12px",
  }),
  ServerSeparator: css({
    height: 2,
    width: 32,
    borderRadius: 1,
    backgroundColor: Colors.BackgroundModifierAccent,
    marginBottom: 8,
    marginLeft: "auto",
    marginRight: "auto",
  }),
  ServerCreationModal: css({
    backgroundColor: Colors.Light.BackgroundPrimary,
    height: "100%",
    display: "flex",
    flexDirection: "column",

    ".upper": {
      position: "relative",
      padding: "24px 16px 0",
      height: 110,
      h1: {
        textAlign: "center",
        marginBottom: 12,
        color: Colors.Light.HeaderPrimary,
      },
      ".tagline": {
        textAlign: "center",
        fontSize: 15,
        marginBottom: 2,
        color: Colors.Light.HeaderSecondary,
      },
      ".disclaimer": {
        textAlign: "center",
        fontSize: 12,
        color: Colors.Light.TextMuted,
      },
      ".close-controls": {
        color: Colors.Light.HeaderSecondary,
        position: "absolute",
        top: 20,
        right: 32,
        cursor: "pointer",
      },
    },
    ".middle": {
      flex: 1,
      // backgroundColor: Colors.BackgroundPrimary,
      padding: 16,
    },
    ".lower": {
      height: 110,
    },
  }),
};

interface ServerBarState {
  newModalOpen: boolean;
}

enum ServerBarAction {
  OpenModal,
  CloseModal,
}

const ServerBuffer = () => <div css={ServerBarStyles.ServerSeparator}></div>;

export const ServerBar: React.FC = () => {
  // Local state
  const initialState: ServerBarState = {
    newModalOpen: false,
  };
  const [state, dispatch] = useReducer(
    (state: ServerBarState, action: ServerBarAction) => {
      switch (action) {
        case ServerBarAction.OpenModal:
          return { ...state, newModalOpen: true };
        case ServerBarAction.CloseModal:
          return { ...state, newModalOpen: false };
        default:
          return state;
      }
    },
    initialState
  );

  return (
    <aside css={ServerBarStyles.Wrapper}>
      <GenericModal
        width={440}
        height={558}
        onClose={() => dispatch(ServerBarAction.CloseModal)}
        open={state.newModalOpen}
      >
        <div css={ServerBarStyles.ServerCreationModal}>
          <div className="upper">
            <h1>Create a server</h1>
            <p className="tagline">
              A Discordium server can be for, or about, anything!
            </p>
            <p className="disclaimer">
              (Like programming, nice memes, or antique chicken coops)
            </p>
            <div
              onClick={() => dispatch(ServerBarAction.CloseModal)}
              role="close"
              className="close-controls"
            >
              <i className="fa fa-times" />
            </div>
          </div>
          <div className="middle">
            <form onSubmit={(e) => e.preventDefault()}>
              <input type="file" />
            </form>
          </div>
          <div className="lower"></div>
        </div>
      </GenericModal>

      <UpperList />
      {/* DMs/other notifications go here */}
      <ServerBuffer />
      {/* Regular servers go here */}
      <ServerList />
      {/* Create server button */}
      <ServerListItem
        onClick={() => dispatch(ServerBarAction.OpenModal)}
        isActive={state.newModalOpen}
        isAddServer
      />
      {/* <ServerBuffer /> */}
      {/* Maybe explore button? - could be out of realistic scope */}
    </aside>
  );
};
