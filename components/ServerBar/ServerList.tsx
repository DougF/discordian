import { useRouter } from "next/router";
import { useServersState } from "../Providers/ServersProvider";
import { ServerListItem } from "./ServerListItem";

export const ServerList: React.FC = () => {
  const router = useRouter();
  const { servers } = useServersState();

  const currentServer = router.query.all ? router.query.all[0] : "no match";
  return (
    <div>
      {servers && // servers variable is null initially
        servers.map((server) => (
          <ServerListItem
            key={server._id}
            serverId={server._id}
            isActive={currentServer === server._id}
          />
        ))}
    </div>
  );
};
