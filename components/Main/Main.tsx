import { css } from "@emotion/react";
import React from "react";
import { Colors } from "../../styles/colors";

const MainStyles = {
  Wrapper: css({
    background: Colors.BackgroundPrimary,
    flex: 1,
  }),
};

export const Main: React.FC = () => {
  return <main css={MainStyles.Wrapper}>Main content area</main>;
};
