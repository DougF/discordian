import React, {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useState,
} from "react";
import { User } from "../../shared/generated";

interface UserState {
  user: User | null;
  setUser: Dispatch<SetStateAction<User | null>>;
}

const UserStateContext = createContext<UserState | undefined>(undefined);

export const useUserState = () => {
  const context = useContext(UserStateContext);
  if (!context) {
    throw new Error("[NO CONTEXT ERROR] Cannot find provider for user state");
  }

  return context;
};

export const UserStateProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);

  return (
    <UserStateContext.Provider value={{ user, setUser }}>
      {children}
    </UserStateContext.Provider>
  );
};
