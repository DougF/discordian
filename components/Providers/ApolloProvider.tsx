import React from "react";

import {
  ApolloClient,
  ApolloProvider as APProvider,
  InMemoryCache,
} from "@apollo/client";

// This assumes that the function routes are served on the same hostname
// (both vercel and netlify do this)
const getBaseUrl = () => {
  // Client side could be broken by CORS
  if (typeof window !== "undefined") {
    return process.env.NODE_ENV === "production"
      ? `https://${window.location.host}`
      : process.env.NEXT_PUBLIC_BASE_URI;
  }

  // Server side shouldn't care
  return process.env.NEXT_PUBLIC_BASE_URI;
};

const apolloClient = new ApolloClient({
  uri: `${getBaseUrl()}/api/graphql`,
  cache: new InMemoryCache(),
});

export const ApolloProvider: React.FC = ({ children }) => {
  return <APProvider client={apolloClient}>{children}</APProvider>;
};
