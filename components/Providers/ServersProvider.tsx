import type React from "react";
import {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useState,
} from "react";
import { Server } from "../../shared/generated";

interface ServersState {
  servers: Server[] | null;
  setServers: Dispatch<SetStateAction<Server[] | null>>;
}

const ServersContext = createContext<ServersState | undefined>(undefined);

export const useServersState = () => {
  const context = useContext(ServersContext);
  if (!context) {
    throw new Error(
      "[NO CONTEXT ERROR] Cannot find provider for servers state"
    );
  }

  return context;
};

export const ServersStateProvider: React.FC = ({ children }) => {
  const [servers, setServers] = useState<Server[] | null>(null);
  return (
    <ServersContext.Provider value={{ servers, setServers }}>
      {children}
    </ServersContext.Provider>
  );
};
